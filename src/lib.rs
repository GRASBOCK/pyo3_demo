use pyo3::prelude::*;
mod demo;


/// Formats the sum of two numbers as string.
#[pyfunction]
fn sum_as_string(a: usize, b: usize) -> PyResult<String> {
    Ok(demo::sum_as_string(a, b))
}

/// A Python module implemented in Rust.
#[pymodule]
fn demo_rs(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(sum_as_string, m)?)?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use pyo3::{prelude::*, types::IntoPyDict};

    #[test]
    fn it_works_externally() -> PyResult<()>{
        // call python from rust
        Python::with_gil(|py| {
            // import python module
            let locals = [("demo_rs", py.import("demo_rs")?)].into_py_dict(py);
            // execute code
            let code = "demo_rs.sum_as_string(1, 2)";
            let sum_as_string: String = py
                .eval(code, None, Some(&locals))?
                .extract()?; // the type to be extracted is hinted by the ": String" next to the variable declaration
            // assert, that the result is true
            assert_eq!(&sum_as_string, "3");
            // return an Ok, with no contents
            Ok(())
        })
    }
}
