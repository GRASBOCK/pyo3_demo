/// takes two integers and returns the sum as a string
pub fn sum_as_string(a: usize, b: usize) -> String {
    (a + b).to_string()
}

#[cfg(test)]
mod tests {
    use crate::demo::sum_as_string;

    #[test]
    fn it_works_internally() {
        assert_eq!(sum_as_string(1, 2), "3");
    }
}
