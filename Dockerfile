# builder base image
FROM python:3.12.0rc3 as build

# get Rust
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --profile minimal
ENV PATH="/root/.cargo/bin:${PATH}"

# get maturin for Rust -> python lib conversion
RUN pip install maturin

# build dependencies (to utilize docker caching)
WORKDIR /demo_rs
COPY Cargo.toml .
RUN mkdir src && \
    echo "" > src/lib.rs && \
    cargo build --lib --release

# build library
COPY . .
RUN maturin build

# install library on system for the testing image
RUN pip install -t /demo_rs_package .
ENV PYTHONPATH=$PYTHONPATH:/demo_rs_package

############################################################
# final python base image to get a small final image 
FROM python:3.12.0rc3-slim as final

# use package from builder and point to it
COPY --from=build /demo_rs_package /demo_rs
ENV PYTHONPATH=$PYTHONPATH:/demo_rs

# copy script to execute
COPY test.py main.py

# the command to execute on run
CMD python3 main.py