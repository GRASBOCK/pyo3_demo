# Python x Rust interop
This is an example library to demonstrate how rust can be used to create a python library

Note that currently type hints are not generated automatically. They have to be implemented in the pyo3_test.pyi file.
## Installing locally
### Linux
Install dependencies:
```sh
# install rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
# install library creator
pip install maturin
```
Clone the repo and execute inside of it:
```sh
# let maturin build the library
maturin build
# install this library to the system
pip install .
```

Execute python code using the library like so
```sh
python3 test.py
```
## docker image
To build and run the docker image
```sh
docker build -t demo_rs . && docker run demo_rs
```

# Usage 

Execute the test.py
```python
import demo_rs
print(demo_rs.sum_as_string(1, 2))
```
Output is the string "3"

# ToDos
- make gitlab CI images smaller
- make gitlab CI faster (probably slow because of large image size)